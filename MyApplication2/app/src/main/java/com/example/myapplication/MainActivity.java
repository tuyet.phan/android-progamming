package com.example.myapplication;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private EditText mEditA, mEditB;
    private TextView mResult;
    private double ma, mb;
    private ListView lvResult;
    ArrayList<String> listOfResults;
    ArrayAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.caculation);
        mEditA = findViewById(R.id.edt_a);
        mEditB = findViewById(R.id.edt_b);
        mResult = findViewById(R.id.tv_result);
        lvResult = findViewById(R.id.lv_result);
        listOfResults = new ArrayList<String>();
        adapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,listOfResults);
        lvResult.setAdapter(adapter);
    }
    public void clickOnButtonAdd(View v)
    {
        try {
            ma = Double.parseDouble(mEditA.getText().toString());
            mb = Double.parseDouble(mEditB.getText().toString());
        }
        catch (Exception e)
        {
            Toast.makeText(this, "Just enter number!", Toast.LENGTH_SHORT).show();
        }
        String plus = Double.toString(ma+mb);
        mResult.setText(mEditA.getText().toString() + " + " + mEditB.getText().toString() + " = " + plus);
        listOfResults.add(""+ma +" + "+mb+" = "+ (ma+mb));
        adapter.notifyDataSetChanged();
    }
    public void clickOnButtonSub(View v)
    {
        try {
            ma = Double.parseDouble(mEditA.getText().toString());
            mb = Double.parseDouble(mEditB.getText().toString());
        }
        catch (Exception e)
        {
            Toast.makeText(this, "Just enter number!", Toast.LENGTH_SHORT).show();
        }
        String sub = Double.toString(ma-mb);
        mResult.setText( mEditA.getText().toString() + " - " + mEditB.getText().toString() + " = " + sub);
        listOfResults.add(""+ma +" - "+mb+" = "+ (ma-mb));
        adapter.notifyDataSetChanged();
    }
    public void clickOnButtonMul(View v)
    {
        try {
            ma = Double.parseDouble(mEditA.getText().toString());
            mb = Double.parseDouble(mEditB.getText().toString());
        }
        catch (Exception e)
        {
            Toast.makeText(this, "Just enter number!", Toast.LENGTH_SHORT).show();

        }
        String mul = Double.toString(ma*mb);
        mResult.setText(mEditA.getText().toString() + " x " + mEditB.getText().toString() + " = " + mul);
        listOfResults.add(""+ma +" x "+mb+" = "+ (ma*mb));
        adapter.notifyDataSetChanged();
    }
    public void clickOnButtonDiv(View v)
    {
        try {
            ma = Double.parseDouble(mEditA.getText().toString());
            mb = Double.parseDouble(mEditB.getText().toString());
        }
        catch (Exception e)
        {
            Toast.makeText(this, "Just enter number!", Toast.LENGTH_SHORT).show();
        }
        String div = Double.toString(ma/mb);
        mResult.setText(mEditA.getText().toString() + " / " + mEditB.getText().toString() + " = " + div);
        listOfResults.add(""+ma +" / "+mb+" = "+ (ma/mb));
        adapter.notifyDataSetChanged();
    }
}
