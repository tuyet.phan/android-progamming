package com.example.contactv;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import de.hdodenhof.circleimageview.CircleImageView;
import model.Contact;

public class Edit extends Activity {
private TextView txt_name;
private TextView txt_moblie;
private CircleImageView civ_Ava;
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_contact);
        txt_name = (TextView) findViewById(R.id.txt_name);
        txt_moblie = (TextView) findViewById(R.id.txt_mobile);
        civ_Ava = (CircleImageView) findViewById(R.id.civ_avatar);

        Intent intent = getIntent();
        Bundle bundle = intent.getBundleExtra("data");

        if( bundle != null){
            Contact contact = (Contact) bundle.getSerializable("person");
            txt_name.setText(contact.getName());
            txt_moblie.setText(contact.getMobile());
            civ_Ava.setImageResource(contact.getResId(R.drawable.class));
        }
    }

    public void onReturnClass(View v) {
        Intent addIntent = new Intent(Edit.this, MainActivity.class);
        startActivity(addIntent);
    }

}

