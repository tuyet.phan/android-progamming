package com.example.contactv;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Add extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_contact);
    }

    public void onReturnClass(View v) {
        Intent addIntent = new Intent(Add.this, MainActivity.class);
        startActivity(addIntent);
    }

}
