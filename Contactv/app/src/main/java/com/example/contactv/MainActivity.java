package com.example.contactv;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

import custom_array.CustomAdapter;
import model.Contact;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.arrayAdapterListView();
    }

    public void onAddClick(View v) {
        Intent addIntent = new Intent(MainActivity.this, Add.class);
        startActivity(addIntent);
    }

    private void arrayAdapterListView() {
        ListView lvContact = findViewById(R.id.lv_contact);
        final ArrayList<Contact> dataList = new ArrayList<>();
        CustomAdapter adapter = new CustomAdapter(this, R.layout.row_listview, dataList);
        Contact contact1 = new Contact("Susan", "hehe", "0182041879");
        Contact contact2 = new Contact("Jenny ", "hihi", "0120758922");
        Contact contact3 = new Contact("Anna", "haha", "249832895");
        dataList.add(contact1);
        dataList.add(contact2);
        dataList.add(contact3);
        lvContact.setAdapter(adapter);

        lvContact.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent editIntent = new Intent(MainActivity.this, Edit.class);
                Bundle transfer = new Bundle();
                transfer.putSerializable("person", dataList.get(i));
                editIntent.putExtra("data",transfer);
                startActivity(editIntent);
            }
        });
    }
}
