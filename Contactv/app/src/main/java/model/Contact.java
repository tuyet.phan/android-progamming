package model;

import java.io.Serializable;
import java.lang.reflect.Field;

public class Contact implements Serializable {
    private String name;
    private String avatar;
    private String mobile;

    public Contact(String name, String avatar, String mobile) {
        this.name = name;
        this.avatar = avatar;
        this.mobile = mobile;
    }

    public String getName() {
        return name;
    }
    public String getMobile() {
        return mobile;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMobile(String mobile) {this.mobile = mobile;}

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
    public int getResId(Class<?> c) {
        try {
            Field idField = c.getDeclaredField(this.avatar);
            return idField.getInt(idField);
        } catch (Exception e) {
            throw new RuntimeException("No resource ID found for: "
                    + this.avatar + " / " + c, e);
        }
    }
}
